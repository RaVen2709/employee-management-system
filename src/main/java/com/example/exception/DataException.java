package com.example.exception;

public class DataException extends Exception {

    public DataException(String errorMessage) {
        super(errorMessage);
    }
}
