package com.example.service;

import com.example.dto.EmployeeDto;
import com.example.exception.MongoDbException;
import com.example.exception.ServiceException;
import java.util.List;

public interface EmployeeManagementService {

    List<EmployeeDto> listEmployees() throws ServiceException;

    EmployeeDto findEmployeeById(String id) throws ServiceException;
}
