package com.example.service.impl;

import com.example.dao.EmployeeManagementDao;
import com.example.dto.EmployeeDto;
import com.example.entity.Employee;
import com.example.exception.MongoDbException;
import com.example.exception.NotFoundException;
import com.example.exception.ServiceException;
import com.example.service.EmployeeManagementService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeManagementServiceImpl implements EmployeeManagementService {

    @Autowired
    private EmployeeManagementDao employeeManagementDao;

    @Override
    public List<EmployeeDto> listEmployees() throws ServiceException {
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        try {
            List<Employee> employeeList = employeeManagementDao.listEmployees();
            employeeList.parallelStream().forEach(employee -> {
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.setId(employee.getId());
                employeeDto.setCode(employee.getCode());
                employeeDto.setName(employee.getName());
                employeeDto.setStatus(employee.getStatus());
                employeeDto.setCreatedDate(employee.getCreatedDate());
                employeeDtoList.add(employeeDto);
            });
        } catch (MongoDbException mongoDbException) {
            throw new ServiceException("Error while processing data", mongoDbException);
        }
        return employeeDtoList;
    }

    @Override
    public EmployeeDto findEmployeeById(String id) throws ServiceException {
        EmployeeDto employeeDto = new EmployeeDto();
        try {
            Employee employee = employeeManagementDao.findEmployeeById(id);
            employeeDto.setId(employee.getId());
            employeeDto.setCode(employee.getCode());
            employeeDto.setName(employee.getName());
            employeeDto.setStatus(employee.getStatus());
            employeeDto.setCreatedDate(employee.getCreatedDate());
        } catch (NotFoundException notFoundException) {
            throw new ServiceException("Employee not found", notFoundException);
        }
        return employeeDto;
    }
}
