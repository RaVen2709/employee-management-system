package com.example.dao;

import com.example.entity.Employee;
import com.example.exception.MongoDbException;
import com.example.exception.NotFoundException;
import java.util.List;

public interface EmployeeManagementDao {

    String _ID = "_id";

    List<Employee> listEmployees() throws MongoDbException;

    Employee findEmployeeById(String id) throws NotFoundException;
}
