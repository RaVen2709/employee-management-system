package com.example.dao.impl;

import com.example.dao.EmployeeManagementDao;
import com.example.entity.Employee;
import com.example.exception.MongoDbException;
import com.example.exception.NotFoundException;
import com.mongodb.MongoException;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeManagementDaoImpl implements EmployeeManagementDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Employee> listEmployees() throws MongoDbException {
        try {
            return mongoTemplate.findAll(Employee.class);
        } catch (MongoException mongoException) {
            throw new MongoDbException("Error while fetching data", mongoException);
        }
    }

    @Override
    public Employee findEmployeeById(String id) throws NotFoundException {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(_ID).is(new ObjectId(id)));
            return mongoTemplate.findOne(query, Employee.class);
        } catch (MongoException mongoException) {
            throw new NotFoundException("Employee not found", mongoException);
        }
    }
}
