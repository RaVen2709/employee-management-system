package com.example.controller;

import static com.example.enums.APIResponseStatus.SUCCESS;

import com.example.controller.response.APIResponse;
import com.example.dto.EmployeeDto;
import com.example.exception.MongoDbException;
import com.example.exception.ServiceException;
import com.example.service.EmployeeManagementService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee-management")
public class EmployeeManagementController {

    @Autowired
    private EmployeeManagementService employeeManagementService;

    @GetMapping("/")
    public ResponseEntity<APIResponse> listEmployees() throws ServiceException {
        APIResponse response = new APIResponse();
        response.setStatus(SUCCESS.getDisplayName());
        response.setData(employeeManagementService.listEmployees());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<APIResponse> findEmployeeById(@PathVariable(value = "id") String id)
        throws ServiceException {
        APIResponse response = new APIResponse();
        response.setStatus(SUCCESS.getDisplayName());
        response.setData(employeeManagementService.findEmployeeById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
